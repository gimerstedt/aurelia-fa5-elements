import * as fs from 'fs'
import * as Case from 'case'
import { promisify } from 'util'

const root = './node_modules/@fortawesome/fontawesome-free/svgs/'
const paths = [root + 'brands', root + 'regular', root + 'solid']
const outDir = './dist'

const ls = promisify(fs.readdir)
const read = promisify(fs.readFile)
const write = promisify(fs.writeFile)

const wrapInTemplate = (content: string) => {
  const a = content.split('>')[0]
  const rest = content.substr(a.length)
  const c = `${a} style.bind="style"${rest}`
  return `<template style="display: inline-block;">${c}</template>`
}

const getTs = (name: string) => {
  const className = Case.pascal(name)
  return `import { bindable } from 'aurelia-framework'

export class ${className} {
  @bindable color: string

  style

  bind() {
    this.updateStyle()
  }

  updateStyle() {
    this.style = {
      width: '100%',
      height: '100%',
      fill: this.color,
    }
  }

  colorChanged() {
    this.updateStyle()
  }
}`
}

const createElement = async (svg: ISvg): Promise<IElement> => {
  const name = 'fa-' + svg.name.substr(0, svg.name.length - 4)

  const fileContents = await read(svg.path)
  const html = wrapInTemplate(fileContents.toString())
  const ts = getTs(name)

  return {
    name,
    html,
    ts,
  }
}

const main = async () => {
  const svgs: ISvg[] = []
  for (const path of paths) {
    const names = await ls(path)
    for (const name of names) svgs.push({ path: `${path}/${name}`, name: name })
  }
  const elements: IElement[] = []
  for (const svg of svgs) {
    const element = await createElement(svg)
    elements.push(element)
  }
  for (const el of elements) {
    await write(`${outDir}/${el.name}.ts`, el.ts)
    await write(`${outDir}/${el.name}.html`, el.html)
  }
}

main()

interface ISvg {
  name: string
  path: string
}

interface IElement {
  name: string
  ts: string
  html: string
}
