import { Aurelia } from 'aurelia-framework'
import { PLATFORM } from 'aurelia-pal'

export function configure(aurelia: Aurelia) {
  aurelia.use
    .standardConfiguration()
    .feature(PLATFORM.moduleName('resources/index'))

  return aurelia.start().then(() => aurelia.setRoot(PLATFORM.moduleName('app')))
}
